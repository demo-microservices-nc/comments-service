package com.example.usersservice.db;

public class Comment {
    private String content;

    private String postId;

    public Comment(String content, String postId) {
        this.content = content;
        this.postId = postId;
    }

    public String getPostId() {
        return postId;
    }

    public String getContent() {
        return content;
    }
}
