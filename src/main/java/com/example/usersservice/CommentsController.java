package com.example.usersservice;

import com.example.usersservice.db.Comment;
import com.example.usersservice.db.CommentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class CommentsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommentsController.class);

    private final CommentRepository commentRepository;

    @Autowired
    public CommentsController(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @RequestMapping(method = GET, value = "/addComment/{postId}/{content}")
    public Comment addComment(@PathVariable String postId, @PathVariable String content) {
        Comment comment = new Comment(content, postId);
        commentRepository.save(comment);

        LOGGER.info("New comment under post - {} was created with content - {}",
                postId, content);

        return comment;
    }

    @RequestMapping(method = GET, value = "/getById/{postId}")
    public List<Comment> getCommentsByPost(@PathVariable String postId) {
        return commentRepository.findByPostId(postId);
    }
}
